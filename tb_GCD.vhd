LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY TB_GCD iS
END TB_GCD;

ARCHITECTURE BEHAVIORAL OF TB_GCD IS

	COMPONENT GCD
		Generic(
			DataLength	: Integer
		);
		Port(
		clk		: IN	std_logic;
		VIn		: IN	std_logic;
		VOut	: OUT	std_logic;
		x, y	: IN	unsigned(7 downto 0);
		RSLT	: OUT	unsigned(7 downto 0)
		);
	End Component;

	--Inputs
   signal clk : std_logic := '0';
   
   -- Clock period definitions
   constant clk_period : time := 10 ns;
   
   Signal Vin	: std_logic;
   Signal Vout	: std_logic;
   Signal Xint	: unsigned(7 downto 0);
   Signal Yint	: unsigned(7 downto 0);
   Signal Rst	: unsigned(7 downto 0);

BEGIN

	tb: GCD
	Generic Map(DataLength => 8)
	Port Map(
		clk=>clk, VIn=>Vin, VOut=>Vout,
		x=>Xint, y=>Yint, RSLT=>Rst
	);

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
		Vin		<=	'1';
		Xint		<=	to_unsigned(24, 8);
		Yint		<=	to_unsigned(15, 8);
      wait for clk_period;
		Vin		<=	'0';
      
	  wait for 55 ns;
	  
	  Vin		<=	'1';
	  Xint		<=	to_unsigned(46, 8);
	  Yint		<=	to_unsigned(16, 8);
      wait for clk_period;
	  Vin		<=	'0';
	  
	  wait for 110 ns;
	  
	  Vin		<=	'1';
	  Xint		<=	to_unsigned(65, 8);
	  Yint		<=	to_unsigned(25, 8);
      wait for clk_period;
	  Vin		<=	'0';

	  wait for 70 ns;
	  
	  Vin		<=	'1';
	  Xint		<=	to_unsigned(127, 8);
	  Yint		<=	to_unsigned(51, 8);
      wait for clk_period;
	  Vin		<=	'0';
	  
	  wait for 300 ns;
	  
	  Vin		<=	'1';
	  Xint		<=	to_unsigned(66, 8);
	  Yint		<=	to_unsigned(36, 8);
      wait for clk_period;
	  Vin		<=	'0';
	  
	  wait for 80 ns;
	  
	  Vin		<=	'1';
	  Xint		<=	to_unsigned(73, 8);
	  Yint		<=	to_unsigned(73, 8);
      wait for clk_period;
	  Vin		<=	'0';
	  
      wait;
   end process;

END BEHAVIORAL;