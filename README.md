Greatest common divisor

Author: masoud mahjoubi


The algorithm that was used:

GCD(X, Y, Z)
IF (X = Y) THEN Z <= X; ELSE
IF X>Y THEN
S <= X-Y; GCD(Y, S, C);
ELSE
R <= Y-X; GCD(X, R, C);
END IF;
END IF;


For large numbers, eight bits of the number are entered into the module every clock.
And during this time the Vin must be high.