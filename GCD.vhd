LIBRARY	IEEE;
USE	IEEE.STD_LOGIC_1164.ALL;
USE	IEEE.NUMERIC_STD.ALL;

Entity GCD is
	Generic(
		DataLength	: Integer	:= 64
		);
	port(
		clk		: IN	std_logic;
		VIn		: IN	std_logic;
		VOut	: OUT	std_logic;
		x, y	: IN	unsigned(7 downto 0);
		RSLT	: OUT	unsigned(7 downto 0));
end GCD;

Architecture Result of GCD is

	Function log2(N : integer)
	Return Integer is
	variable temp, log	: Integer := 0;
	Begin
		temp:=N;
		log:=0;
		while (temp /= 0) loop
			temp:=temp/2;
			log:=log+1;
		end loop;
		log:=log-1;
		return log;
	end log2;

	Signal cntData		: unsigned(log2(DataLength/8) downto 0)	:= to_unsigned(DataLength/8, log2(DataLength/8)+1);
	Signal DataCount	: unsigned(log2(DataLength/8) downto 0)	:= to_unsigned(DataLength/8, log2(DataLength/8)+1);
	Signal IntX, IntY	: unsigned(DataLength-1 downto 0)	:= (others=>'0');

begin
	process(clk)
	begin
		if(clk='1' and clk'event) then

			VOut		<=	'0';
			
			IF	(IntX=IntY)	THEN
				if(DataCount<to_integer(cntData)) then
				    if(Vin /= '1') then
				        VOut        <=  '1';
					    RSLT		<=	IntX(7 downto 0);
					    DataCount	<=	DataCount + 1;
					    IntX	<=	IntX srl 8; --logical shift right
					    IntY	<=	IntX srl 8;
					end if;
				end if;
			ELSE
				IF	(IntX>IntY)	THEN	
					IntX		<=	IntX - IntY;
					IntY		<=	IntY;
				ELSE
					IntY		<=	IntY - IntX;
					IntX		<=	IntX;
				END IF;
			END IF;
		
			if(VIn='1') then	-- To change the DataCount is a priority with this IF-Condition
				if(DataCount>0) then
					IntX		<=  IntX(DataLength-1 downto 8) & X;
					IntY		<=  IntY(DataLength-1 downto 8) & Y;
					DataCount	<= DataCount - 1;
				end if;
			end if;
			
		end if;
	end process;
end Result;